import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>LKN & LTT</title>
        <meta name="description" content="Chúc phúc 2 bạn nghen Đầu bự" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        {/* <link rel="icon" href="/favicon.ico" /> */}
        <link rel="stylesheet" type="text/css" href="theme.css"/> 
        <link rel="stylesheet" type="text/css" href="./icons/css/all.min.css"/> 
        <link href="https://fonts.googleapis.com/css?family=Caveat|Quicksand&display=swap" rel="stylesheet"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
      </Head>
      <main>
      <header className="header">
      <h1 className="heading">
        Bông & Trang
        <span className="heading-subtext"> are getting married!</span>
      </h1>
    </header>
    <a data-scroll className="fixed-button" href="#rsvp" id="rsvpButton">RSVP</a> 
    <section className="section">
      <h2 className="sub-heading">
        17-12-2022 
        <br/>
      </h2>
      <p className="details">
        Nghi&Quyên send a warm congratulation to a perfect couple
      </p>
      <div className="red">♥♥♥♥♥♥♥♥♥♥♥♥</div>
      <h2 className="details">
        May the love you share today grow stronger as you grow old together!
      </h2>
    </section>
      </main>
    </>
  )
}
